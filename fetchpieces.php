<?php

# !!! Pour récupérer les pièces, le user d'apache (www-data) doit être mebmbre du group à qui apartiennent les documents.

require('.mongocredentials');
require('genExtraits.php');

# Erreures de post
if(!isset($_POST['submited'])) exit("pas de données postées !");

if($_POST['cotes'] == "") exit('écrire au moins une cote');

# initialisation des variables
$format = $_POST['format'] ?: "diffusion" ;
$cotes = array();
$fichier_cotes_manquantes = tempnam('/tmp','');
$fichier_zip = tempnam('/tmp','');
$list_temp_files = array($fichier_cotes_manquantes, $fichier_zip);
$date = date("Y-m-d");
$zip = new ZipArchive;
$racine = "/mnt/nas_archives";

$cotes_manquantes = fopen($fichier_cotes_manquantes, 'w') or exit("impossible d'écrire sur le serveur");

$create_zip = $zip->open($fichier_zip, ZipArchive::OVERWRITE) ;

if($create_zip != true) exit("Impossible de créer le fichier zip : $create_zip");

# Connexion Mango
$manager = new MongoDB\Driver\Manager($mongocredentials['uri']);
$namespace = $mongocredentials['namespace'];

$query = new MongoDB\Driver\Query(['localisation' => ['$regex' => $format], 'objecttype' => 'digital'], ['projection' => ['cote' => 1, 'localisation' => 1, '_id' => 0]]);

$cotes_dispo = $manager->executeQuery('fjme-db.objets', $query)->toArray();

# converti le tableau d'objets en pure tableau
# ATTENTION le tableau de résultat peut être trop gros et dépasser la memroy_limit de php.ini
# si c'est le cas, cela fait planter le script.
$cotes_dispo = json_decode(json_encode($cotes_dispo), true);

#découpage liste des cotes demandées en tableau
$cotes = preg_split('/\r\n|\r|\n/', $_POST['cotes']);

foreach($cotes as $misc) {
	# extraire les réf d'extrait qui suivent les cotes
	list($cote, $plage) = array_pad(explode(',', $misc, 2), 2, null);

	# Transformation des cotes humaine en cote machine si besoin
	if(strpos($cote, ' ') !== false) {
		# changer cote humaine en cote machine
		list($fonds,$numeros) = explode(' ', $cote, 2);
		$numeros = explode('/', $numeros);
		foreach($numeros as &$numero) {
			$numero = sprintf("%03s", $numero);
		}
		$numeros = implode('-', $numeros);
		$cote = "$fonds-$numeros";		
	}

	# Recherche si une ligne de la BD correspond à la cote ...
	$ligne = array_search($cote, array_column($cotes_dispo, 'cote'));

	# ... si oui
	if($ligne !== false) {

		$chemin_fichier = $racine.$cotes_dispo[$ligne]['localisation'];
		$nom_fichier = basename($chemin_fichier);

		# Si besoin d'extraits, on appel la fonction d'extraction
		if(isset($plage)) list($chemin_fichier, $nom_fichier) = gen_extrait($chemin_fichier, $plage);

		$list_temp_files[] = $chemin_fichier;

		# ajouter fichier au zip
		$zip->addFile($chemin_fichier, $nom_fichier);

	} else {
		# ... sinon, ajouter la cote au fichier text des cotes manquantes
		fwrite($cotes_manquantes, $cote.PHP_EOL);
	}
}

# mettre le fichier text dans le zip si il contient quelque chose
fclose($cotes_manquantes);
if(filesize($fichier_cotes_manquantes) != 0) $zip->addFile($fichier_cotes_manquantes,'_cotes_manquantes.txt');

# Ajout des droits dans le zip et "emballage" du zip
$zip->addFile(dirname(__FILE__).'/pdf/_DROITS_UTILISATION_.pdf','_DROITS_UTILISATION_.pdf');
$zip->close();

#$mysqli->close();

# retourner le fichier zip
header('Content-Description: File Transfer');
header('Content-Type: application/zip');
header('Content-Disposition: attachment; filename="FJME_Extraits_'.$date.'.zip"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: '.filesize($fichier_zip));
ob_clean();
flush();
readfile($fichier_zip);

# suppression des fichiers temporaires
foreach ($list_temp_files as $temp_file) {
	unlink($temp_file);
}

?>
