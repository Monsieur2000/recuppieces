<?php
# fonction d'extraction des extraits de fetchpieces.php

function gen_extrait($chemin_fichier, $plage)
{
    $temp = tempnam('/tmp','');

    $fichier = pathinfo($chemin_fichier);

    # check le type d'extraits
    if(strpos($plage, ':') !== false) {
        # extraits video
        list($db, $fin) = explode(',', $plage);
        $db = trim($db);
        $fin = trim($fin);

        exec("ffmpeg -y -i $chemin_fichier -ss $db -to $fin -f ".$fichier['extension']." -c copy $temp");

        list($db, $fin) = str_replace(':', '', [$db, $fin]);

    } elseif (strpos($plage, '-') !== false) {
        # extraits pdf

        # on récupère les premiers et derniers numéros pour le nom de fichier
	preg_match('/(\d*)[,-].*[,-](\d*)/', $plage, $matches);
        list( , $db, $fin) = $matches;

        #Ajouter page de garde au fonds Delors
        if(strpos($chemin_fichier, '/JD-') !== false) {
            $plage = '1,'.$plage;
        }

        exec("gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -sPageList=$plage -sOutputFile=$temp $chemin_fichier");

    } else {

        return false;
    }

    #retour du fichier et de son nom
    $nom = $fichier['filename']."_".$db."-".$fin.".".$fichier['extension'];
    #$nom = $fichier['filename']."_".$matches[0].".".$fichier['extension'];

    return array($temp, $nom);

}

?>
